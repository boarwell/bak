package main

// 2017-10-27 作成
// 2017-10-27:
// とりあえずちゃんと動く、という感じです。リファクタリング待ってます。

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

func main() {
	// bakというディレクトリがなければ作る
	// bakという通常ファイルが存在したらエラーを出して終了
	bakStat, errB := os.Stat("bak")

	if os.IsNotExist(errB) {
		os.Mkdir("bak", 0755)
	} else if !(bakStat.IsDir()) {
		log.Fatal("there is file named bak")
	}

	dir, _ := os.Getwd()
	fileinfos, _ := ioutil.ReadDir(dir)
	var src *os.File
	var dst *os.File
	var n string

	r := regexp.MustCompile(`_\d{4}-\d{2}-\d{2}\.xlsx$`)

	t := time.Now()
	day := fmt.Sprintf("_%d-%02d-%02d", t.Year(), t.Month(), t.Day())

	for _, v := range fileinfos {
		// エクセルファイルだったらコピー
		// すでに名前の末尾に_yyyy-mm-ddがついていたらスキップ
		if filepath.Ext(v.Name()) == ".xlsx" && !(r.MatchString(v.Name())) {
			src, _ = os.Open(v.Name())
			n = strings.Trim(v.Name(), ".xlsx") + day + ".xlsx"

			dst, _ = os.Create(filepath.Join("bak", n))

			io.Copy(dst, src)

		}
	}
}
